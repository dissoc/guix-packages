(define-module (dissoc packages radio)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages algebra)
  #:use-module (gnu packages autotools)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages python)
  #:use-module (gnu packages python-science)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages qt)
  #:use-module (guix build utils)
  #:use-module (guix build-system cmake)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system python)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix packages))


(define-public nanovna-saver
  (package
   (name "nanovna-saver")
   (version "0.3.4")
   (source (origin
            (method git-fetch)
            (uri (git-reference (url "https://github.com/NanoVNA-Saver/nanovna-saver.git")
                                (commit (string-append "v" version))))
            (sha256
             (base32
              "1swajwhs3370d3gmccrqav7373r4aqnkkmnwxl88psfwsczjkwql"))))
   (build-system python-build-system)
   (arguments
    `(#:tests? #f)) ;;TODO: fix failing tests
   (inputs
    `(("python-pyserlai" ,python-pyserial)
      ("python-pyqt" ,python-pyqt)
      ("python-numpy" ,python-numpy)
      ("python-scipy" ,python-scipy)
      ("python-cython" ,python-cython)
      ("python-setuptools" ,python-setuptools)
      ("python" ,python)))
   (synopsis "Desktop tool for the NanoVNA")
   (description
    "A multiplatform tool to save Touchstone files from the NanoVNA, sweep frequency spans in segments to gain more than 101 data points, and generally display and analyze the resulting data.")
   (home-page "https://github.com/NanoVNA-Saver/nanovna-saver")
   (license license:gpl3+)))
