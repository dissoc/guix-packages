(define-module (dissoc packages monitoring)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages java)
  #:use-module (gnu packages monitoring)
  #:use-module (gnu packages)
  #:use-module (guix build-system gnu)
  #:use-module (gnu packages base)
  #:use-module (guix download)
  #:use-module (gnu packages pcre)
  #:use-module (guix packages)
  #:use-module (guix utils))



(define-public zabbix-agent-jmx
  (package
   (name "zabbix-agent-jmx")
   (version "5.2.2")
   (source
    (origin
     (method url-fetch)
     (uri (string-append
           "https://cdn.zabbix.com/zabbix/sources/stable/"
           (version-major+minor version) "/zabbix-" version ".tar.gz"))
     (sha256
      (base32 "16sqx5hrqkciwnl6xs1b8mwf0fz7x9f4214jhj9s86w0mqiscw8g"))))
   (build-system gnu-build-system)
   (arguments
    `(#:configure-flags
      (list "--enable-java"
            "--enable-agent"
            (string-append "--with-iconv="
                           (assoc-ref %build-inputs "libiconv"))
            (string-append "--with-libpcre="
                           (assoc-ref %build-inputs "pcre")))
      #:phases
      (modify-phases %standard-phases
                     (add-after 'install 'patch-java
                                (lambda* (#:key inputs outputs #:allow-other-keys)
                                  (let* ((out (assoc-ref outputs "out"))
                                         (java-bin (string-append (assoc-ref inputs "openjdk:jdk")
                                                                  "/bin/java")))
                                    (wrap-program (string-append out "/sbin/zabbix_java/startup.sh")
                                                  `("JAVA" ":" = (,java-bin)))
                                    #t))))))
   (inputs
    `(("libiconv" ,libiconv)
      ("openjdk:jdk" ,openjdk14 "jdk")
      ("pcre" ,pcre)))
   (home-page "https://www.zabbix.com/")
   (synopsis "Distributed monitoring solution (client-side agent)")
   (description "This package provides a distributed monitoring
solution (client-side agent)")
   (license license:gpl2)))

(define-public zabbix-jmx-agent
  (package
   (name "zabbix-jmx-agent")
   (version "5.2.2")
   (source
    (origin
     (method url-fetch)
     (uri (string-append
           "https://cdn.zabbix.com/zabbix/sources/stable/"
           (version-major+minor version) "/zabbix-" version ".tar.gz"))
     (sha256
      (base32 "16sqx5hrqkciwnl6xs1b8mwf0fz7x9f4214jhj9s86w0mqiscw8g"))))
   (build-system gnu-build-system)
   (arguments
    `(#:configure-flags
      (list "--enable-java")
      #:phases
      (modify-phases %standard-phases
                     (add-after 'install 'patch-java
                                (lambda* (#:key inputs outputs #:allow-other-keys)
                                  (let* ((out (assoc-ref outputs "out"))
                                         (java-bin (string-append (assoc-ref inputs "openjdk:jdk")
                                                                  "/bin/java")))

                                    (substitute* (string-append out "/sbin/zabbix_java/lib/logback.xml" )
                                                 (("/tmp/zabbix_java.log")
                                                  "/var/log/zabbix/jmx-agent-service.log"))

                                    (substitute* (string-append out "/sbin/zabbix_java/startup.sh" )
                                                 (("cd `dirname ")
                                                  (string-append "##cd `dirname"
                                                                 "\n"
                                                                 "cd "
                                                                 (string-append out "/sbin/zabbix_java/")
                                                                 "\n"
                                                                 "###"))
                                                 ((". ./settings.sh")
                                                  "##. ./settings.sh")


                                                 (("# verify that the gateway started successfully")
                                                  "echo $COMMAND_LINE")

                                                 (("cat")
                                                  (string-append (assoc-ref inputs "coreutils")
                                                                 "/bin/cat"))
                                                 (("rm -f")
                                                  (string-append (assoc-ref inputs "coreutils")
                                                                 "/bin/rm -f"))
                                                 (("touch")
                                                  (string-append (assoc-ref inputs "coreutils")
                                                                 "/bin/touch")))
                                    (wrap-program (string-append out "/sbin/zabbix_java/startup.sh")
                                                  `("JAVA" ":" = (,java-bin)))
                                    #t))))))
   (inputs
    `(("openjdk:jdk" ,openjdk14 "jdk")
      ("coreutils" ,coreutils)
      ))
   (home-page "https://www.zabbix.com/")
   (synopsis "Distributed monitoring solution (client-side agent)")
   (description "This package provides a distributed monitoring
solution (client-side agent)")
   (license license:gpl2)))



;; (define-public zabbix-agent-jmx
;;   (package
;;    (inherit zabbix-agentd)
;;    (name "zabbix-agent-jmx")

;;    (arguments
;;     (substitute-keyword-arguments
;;      (package-arguments zabbix-agentd)
;;      ((#:configure-flags flags)
;;       `(cons* "--enable-java" ,flags))

;;      ((#:phases phases)
;;       `(modify-phases ,phases
;;                       (add-after 'install 'patch-java
;;                                  (lambda* (#:key inputs outputs #:allow-other-keys)
;;                                    (let* ((out (assoc-ref outputs "out"))
;;                                           (java-bin (string-append (assoc-ref inputs "openjdk:jdk")
;;                                                                    "/bin/java")))
;;                                      (wrap-program (string-append out "/sbin/zabbix_java/startup.sh")
;;                                                    `("JAVA" ":" = (,java-bin)))
;;                                      #t)))))))
;;    (inputs
;;     `(("openjdk:jdk" ,openjdk14 "jdk")
;;       ,@(package-inputs zabbix-agentd)))
;;    (synopsis "Distributed monitoring solution (server-side)")
;;    (description "This package provides a distributed monitoring
;; solution (server-side)")))

;;zabbix-java-gateway
