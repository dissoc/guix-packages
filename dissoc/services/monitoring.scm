(define-module (dissoc services monitoring)
  #:use-module (dissoc packages monitoring)

  #:use-module (gnu services)
  #:use-module (gnu services configuration)
  #:use-module (gnu services shepherd)
  #:use-module (gnu services web)
  #:use-module (gnu packages admin)
  #:use-module (gnu packages monitoring)
  #:use-module (gnu system shadow)
  #:use-module (guix gexp)
  #:use-module (guix packages)
  #:use-module (guix records)
  #:use-module (guix utils)
  #:use-module ((guix ui) #:select (display-hint G_))
  #:use-module (ice-9 match)
  #:use-module (ice-9 rdelim)
  #:use-module (srfi srfi-26)
  #:use-module (srfi srfi-35)




  ;; #:use-module (guix build union)
  ;; #:use-module (guix modules)
  ;; #:use-module (srfi srfi-1)

  #:export (<zabbix-jmx-agent-configuration>

            zabbix-jmx-agent-service-type
            zabbix-jmx-agent-service)

  )



(define-record-type* <zabbix-jmx-agent-configuration>
  zabbix-jmx-agent-configuration  make-zabbix-jmx-agent-configuration
  zabbix-jmx-agent-configuration?
  (package            zabbix-jmx-agent-configuration-package
                      (default zabbix-jmx-agent))
  (user               zabbix-jmx-agent-configuration-user
                      (default  "zabbix"))
  (group              zabbix-jmx-agent-configuration-group
                      (default "zabbix"))
  (listen-ip          zabbix-jmx-agent-configuration-listen-ip
                      (default "0.0.0.0"))
  (listen-port        zabbix-jmx-agent-configuration-listen-port
                      (default 10052))
  (pid-file           zabbix-jmx-agent-configuration-pid-file
                      (default "/var/run/zabbix/zabbix-jmx-agent.pid"))
  (log-file           zabbix-jmx-agent-configuration-log-file
                      (default "/var/log/zabbix/jmx-agent.log"))
  (start-pollers      zabbix-jmx-agent-configuration-start-pollers
                      (default 5))
  (timeout            zabbix-jmx-agent-configuration-timeout
                      (default 3)))

(define zabbix-jmx-agent-activation
  (match-lambda
    (($ <zabbix-jmx-agent-configuration>)
     #~(begin
         (use-modules (guix build utils)
                      (ice-9 match))
         (let* ((user (getpwnam "zabbix"))
                (pid-dir "/var/run/zabbix/")
                (log-dir "/var/log/zabbix/")
                (uid (passwd:uid user))
                (gid (passwd:gid user)))
           (mkdir-p pid-dir)
           (chown pid-dir uid gid)
           (mkdir-p log-dir)
           (chown log-dir uid gid))))))


(define (zabbix-jmx-agent-account config)
  "Return the user accounts and user groups for CONFIG."
  (list (user-group (name "zabbix") (system? #t))
        (user-account
         (name "zabbix")
         (system? #t)
         (group "zabbix")
         (comment "zabbix privilege separation user")
         (home-directory "/var/empty")
         (shell (file-append shadow "/sbin/nologin")))))


(define (zabbix-jmx-agent-shepherd-service config)
  "Return a <shepherd-service> for Zabbix jmx agent with CONFIG."
  (list (shepherd-service
         (provision '(zabbix-jmx-agent))
         (documentation "Run Zabbix server daemon.")
         (start #~(make-forkexec-constructor
                   (list #$(file-append (zabbix-jmx-agent-configuration-package config)
                                        "/sbin/zabbix_java/startup.sh"))
                   #:user "zabbix"
                   #:group "zabbix"
                   ;;#:pid-file "/var/run/zabbix/zabbix-jmx-agent.pid"
                   #:log-file "/var/log/zabbix/jmx-agent-service.log"
                   #:environment-variables
                   (list (string-append "LISTEN_IP=" "0.0.0.0"
                                        ;;#$(zabbix-jmx-agent-configuration-listen-ip config)
                                        )
                         (string-append "LISTEN_PORT=" "10052"
                                        ;;#$((number->string zabbix-jmx-agent-configuration-listen-port config))

                                        )
                         ;; (string-append "PID_FILE=" "/var/run/zabbix/zabbix-jmx-agent.pid"
                         ;;                ;;#$(zabbix-jmx-agent-configuration-pid-file config)

                         ;;                )
                         (string-append "START_POLLERS=" "5"
                                        ;;#$((number->string zabbix-jmx-agent-configuration-start-pollers config))

                                        )
                         (string-append "TIMEOUT=" "3"
                                        ;;#$((number->string zabbix-jmx-agent-configuration-timeout config))

                                        ))))
         (stop #~(make-kill-destructor)))))


(define zabbix-jmx-agent-service-type
  (service-type
   (name 'zabbix-jmx-agent)
   (extensions
    (list (service-extension shepherd-root-service-type
                             zabbix-jmx-agent-shepherd-service)
          (service-extension account-service-type
                             zabbix-jmx-agent-account)
          (service-extension activation-service-type
                             zabbix-jmx-agent-activation)))
   (default-value (zabbix-jmx-agent-configuration))))


;; (define* (zabbix-java-gateway-service
;;           #:key zabbix-java-gateway zabbix-java-gateway)
;;   "Returns a zabbix-java-gateway service"
;;   (service zabbix-java-gateway-service-type
;;            (zabbix-java-gateway-configuration
;;             (zabbix-java-gateway zabbix-java-gateway))))
