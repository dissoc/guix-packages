(define-module (dissoc services vpn)
  #:use-module (gnu packages admin)
  #:use-module (gnu packages databases)
  #:use-module (gnu services shepherd)
  #:use-module (gnu services)
  #:use-module (gnu system shadow)
  #:use-module (guix build union)
  #:use-module (guix build-system trivial)
  #:use-module (guix gexp)
  #:use-module (guix modules)
  #:use-module (guix packages)
  #:use-module (guix records)
  #:use-module (ice-9 match)
  #:use-module (srfi srfi-1)
  #:use-module (dissoc packages vpn)
  #:export (<zerotier-one-configuration>
            zerotier-one-config-file
            zerotier-one-service-type
            zerotier-one-service))


(define-record-type* <zerotier-one-configuration>
  zerotier-one-configuration make-zerotier-one-configuration
  zerotier-one-configuration?
  (zerotier-one          zerotier-one-configuration-zerotier-one ;<package>
                         (default zerotier-one)))


(define %zerotier-one-accounts
  (list (user-group (name "zerotier-one")
                    (system? #t))
        (user-account
         (name "zerotier-one")
         (group "zerotier-one")
         (system? #t)
         (comment "Zerotier-One server user")
         (home-directory "/var/empty")
         (shell (file-append shadow "/sbin/nologin")))))


;; (define zerotier-one-activation
;;   (match-lambda
;;     (($ <zerotier-one-configuration> zerotier-one)
;;      #~(begin
;;          (use-modules (guix build utils)
;;                       (ice-9 match))
;;          (let ((user (getpwnam "zerotier-one")))
;;            (chown #$data-directory (passwd:uid user) (passwd:gid user)))))))


(define zerotier-one-shepherd-service
  (match-lambda
    (($ <zerotier-one-configuration> zerotier-one)
     (list (shepherd-service
            (provision '(zerotier-one))
            (documentation "Run the Zerotier-One daemon.")
            (requirement '(user-processes loopback))
            (start #~(make-forkexec-constructor
                      (list (string-append #$zerotier-one "/sbin/zerotier-one"))
                      #:user "root" ;;"zerotier-one"
                      #:group "root" ;;"zerotier-one"
                      #:pid-file "/var/lib/zerotier-one/zerotier-one.pid"
                      #:log-file "/var/log/zerotier-one.log"))


            (stop #~(make-kill-destructor)))))))


(define zerotier-one-service-type
  (service-type
   (name 'zerotier-one)
   (extensions
    (list (service-extension shepherd-root-service-type
                             zerotier-one-shepherd-service)
          ;; (service-extension activation-service-type
          ;;                    zerotier-one-activation)
          (service-extension account-service-type
                             (const %zerotier-one-accounts))))
   (default-value (zerotier-one-configuration))))


;; (define* (zerotier-one-service #:key (zerotier-one zerotier-one))
;;   "Returns a zerotier-one service"
;;   (service zerotier-one-service-type
;;            (zerotier-one-configuration
;;             (zerotier-one zerotier-one))))
