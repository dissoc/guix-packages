(define-module (dissoc services databases)
  #:use-module (dissoc packages databases)
  #:use-module (gnu packages admin)
  #:use-module (gnu packages databases)
  #:use-module (gnu services shepherd)
  #:use-module (gnu system shadow)
  #:use-module (guix build union)
  #:use-module (guix build-system trivial)
  #:use-module (guix gexp)
  #:use-module (guix modules)
  #:use-module (guix packages)
  #:use-module (guix records)
  #:use-module (ice-9 match)
  #:use-module (srfi srfi-1)
  #:use-module (gnu services)
  #:export (<cassandra-configuration>
            cassandra-config-file
            cassandra-service-type
            cassandra-service))


(define-record-type* <cassandra-configuration>
  cassandra-configuration make-cassandra-configuration
  cassandra-configuration?
  (cassandra          cassandra-configuration-cassandra ;<package>
                      (default cassandra))
  (data-directory cassandra-configuration-data-directory
                  (default "/var/lib/cassandra")))


(define %cassandra-accounts
  (list (user-group (name "cassandra") (system? #t))
        (user-account
         (name "cassandra")
         (group "cassandra")
         (system? #t)
         (comment "Cassandra server user")
         (home-directory "/var/empty")
         (shell (file-append shadow "/sbin/nologin")))))


(define cassandra-activation
  (match-lambda
    (($ <cassandra-configuration> cassandra data-directory)
     #~(begin
         (use-modules (guix build utils)
                      (ice-9 match))
         (let ((user (getpwnam "cassandra"))
               (pid-dir "/var/run/cassandra/"))
           (mkdir-p #$data-directory)
           (chown #$data-directory (passwd:uid user) (passwd:gid user))
           (mkdir-p pid-dir)
           (chown pid-dir (passwd:uid user) (passwd:gid user)))))))


(define cassandra-shepherd-service
  (match-lambda
    (($ <cassandra-configuration> cassandra data-directory)
     (list (shepherd-service
            (provision '(cassandra))
            (documentation "Run the Cassandra daemon.")
            (requirement '(user-processes loopback))
            (start #~(make-forkexec-constructor
                      (list (string-append  #$cassandra "/bin/cassandra")
                            "-p /var/run/cassandra/cassandra.pid")
                      #:user "cassandra"
                      #:group "cassandra"
                      #:pid-file "/var/run/cassandra/cassandra.pid"
                      #:log-file "/var/log/cassandra.log"
                      #:pid-file-timeout 60))
            (stop #~(make-kill-destructor)))))))


(define cassandra-service-type
  (service-type (name 'cassandra)
                (extensions
                 (list (service-extension shepherd-root-service-type
                                          cassandra-shepherd-service)
                       (service-extension activation-service-type
                                          cassandra-activation)
                       (service-extension account-service-type
                                          (const %cassandra-accounts))))
                (default-value (cassandra-configuration))))


(define* (cassandra-service #:key (cassandra cassandra))
  "Returns a cassandra service"
  (service cassandra-service-type
           (cassandra-configuration
            (cassandra cassandra))))
