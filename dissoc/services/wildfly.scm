(define-module (dissoc services wildfly)
  #:use-module (gnu services)
  #:use-module (gnu services configuration)
  #:use-module (gnu services base)
  #:use-module (gnu services admin)
  #:use-module (gnu services shepherd)
  #:use-module (gnu packages admin)
  ;;#:use-module ((ice-9 ftw) #:select (scandir))
  #:use-module (gnu packages java)
  #:use-module (gnu system shadow)
  #:use-module (dissoc packages wildfly)
  #:use-module (guix records)
  #:use-module (guix gexp)
  #:use-module (guix packages)
  #:export (wildfly-service-type
            wildfly-configuration))

;; JBOSS_PIDFILE
;; JBOSS_HOME
;; JAVA_HOME
;;"$JAVA_OPTS $SERVER_OPTS"
;; JAVA_OPTS='--add-modules java.se'
;; JBOSS_LOG_DIR

;;bin/standalone.sh -c $my_config -b $my_bind

(define-record-type* <wildfly-configuration>
  wildfly-configuration make-wildfly-configuration
  wildfly-configuration?
  (config-dir wildfly-configuration-config-dir
              (default '()))
  (wildfly wildfly-configuration-wildfly ; package
           (default wildfly13)))      ; file-like


(define %wildfly-log-rotations
  (list (log-rotation
         (files (list "/var/log/wildfly.log"))
         (frequency 'daily))))


(define (wildfly-activation config)
  "Return the activation gexp for CONFIG."
  #~(begin
      (use-modules (guix build utils)
                   (ice-9 ftw))
      (let* ((config-dir #$(wildfly-configuration-config-dir config))
            (pw  (getpwnam "wildfly"))
            (uid (passwd:uid pw))
            (gid (passwd:gid pw)))

        (mkdir-p "/etc/wildfly")
        ;; (call-with-output-file "/etc/zabbix/maintenance.inc.php"
        ;;   (lambda (port)
        ;;     (display #$%maintenance.inc.php port)))



        (copy-recursively  config-dir
                           "/var/lib/wildfly/config/")

        (for-each (lambda (f)
                    (chown (string-append "/var/lib/wildfly/config/" f)
                           uid
                           gid))
                  (cdr (cdr (scandir "/var/lib/wildfly/config/"))))

        ;; (copy-file (string-append #$wildfly13 "/opt/wildfly-13.0.0.Final/standalone/configuration/logging.properties")
        ;;            "/tmp/wildfly/config/logging.properties")
        ;; (copy-file (string-append #$wildfly13 "/opt/wildfly-13.0.0.Final/standalone/configuration/application-roles.properties")
        ;;            "/tmp/wildfly/config/application-roles.properties")
        ;; (copy-file (string-append #$wildfly13 "/opt/wildfly-13.0.0.Final/standalone/configuration/application-users.properties")
        ;;            "/tmp/wildfly/config/application-users.properties")
        ;; (copy-file (string-append #$wildfly13 "/opt/wildfly-13.0.0.Final/standalone/configuration/mgmt-groups.properties")
        ;;            "/tmp/wildfly/config/mgmt-groups.properties")
        ;; (copy-file (string-append #$wildfly13 "/opt/wildfly-13.0.0.Final/standalone/configuration/mgmt-users.properties")
        ;;            "/tmp/wildfly/config/mgmt-users.properties")
        ;; (copy-file (string-append #$wildfly13 "/opt/wildfly-13.0.0.Final/standalone/configuration/standalone.xml")
        ;;            "/tmp/wildfly/config/standalone.xml")

        )))

(define (wildfly-account config)
  "Return the user accounts and user groups for CONFIG."
  (list (user-group (name "wildfly") (system? #t))
        (user-account
         (name "wildfly")
         (system? #t)
         (group "wildfly")
         (comment "wildfly privilege separation user")
         (home-directory (string-append "/var/empty"))
         (shell (file-append shadow "/sbin/nologin")))))

(define (wildfly-shepherd-service config)
  (let* ((wildfly (wildfly-configuration-wildfly config)))
    (list (shepherd-service
           (documentation "Runs the wildfly app server")
           (provision '(wildfly))
           (start #~(make-forkexec-constructor
                     (list (string-append #$wildfly "/opt/wildfly-13.0.0.Final/bin/standalone.sh"))
                     #:log-file "/var/log/wildfly.log"
                     #:user "wildfly"
                     #:group "wildfly"
                     #:environment-variables
                     (list
                      ;;NOTE: maybe use JAVA_OPTS from standalone.conf
                      (string-append "JAVA_OPTS="

                                     (string-join '("-Xms64m -Xmx512m -XX:MetaspaceSize=96M"
                                                    "-XX:MaxMetaspaceSize=256m -Djava.net.preferIPv4Stack=true"
                                                    "-Djboss.modules.system.pkgs=$JBOSS_MODULES_SYSTEM_PKGS"
                                                    "-Djava.awt.headless=true"
                                                    "--add-modules java.se"
                                                    "-Djboss.server.log.dir=/tmp/wildfly/log/"
                                                    "-Djboss.server.data.dir=/tmp/wildfly/data/"
                                                    "-Djboss.server.temp.dir=/tmp/wildfly/temp/"
                                                    "-Djboss.server.config.dir=/var/lib/wildfly/config/"
                                                    "-Djboss.server.deploy.dir=/tmp/wildfly/deployments"
                                                    "-Djboss.config.current-history-length=0"
                                                    "-Djboss.config.history-days=0"
                                                    )))
                      (string-append "JAVA_HOME=" #$openjdk14:jdk)
                      (string-append "JBOSS_HOME=" #$wildfly "/opt/wildfly-13.0.0.Final/")
                      (string-append "JBOSS_CONFIG_DIR=/var/lib/wildfly/config/")
                      (string-append "JBOSS_LOG_DIR=/tmp/wildfly/jboss-log/"))))
           (stop #~(make-kill-destructor))))))

(define wildfly-service-type
  (service-type (name 'wildfly)
                (description "Runs the wildfly app server")
                (extensions
                 (list
                  (service-extension shepherd-root-service-type
                                     wildfly-shepherd-service)
                  (service-extension account-service-type
                                     wildfly-account)
                  (service-extension activation-service-type
                                     wildfly-activation)
                  (service-extension rottlog-service-type
                                     (const %wildfly-log-rotations))))
                (default-value
                  (wildfly-configuration))))
